Instruções para iniciar o projeto:

Para instalar o Docker em sua maquina acesse: https://docs.docker.com/install/linux/docker-ce/ubuntu/ (selecione a sua distribuição).

Baixe a imagem em sua maquina com o comando:
    
    docker pull pxemanuel/weblanchonete:1.0
    
Para criar o conteiner use o comando: 
    
    docker container create -p 3000:80 --name weblanchonete pxemanuel/weblanchonete:1.0
    
Para acessar o container use o comando:
    
    docker start weblanchonete

No seu navegador acesse:
    
    http://localhost:3000/
    
Apos a utilizaçã, use o comando para fechar o container:

    docker stop weblanchonete